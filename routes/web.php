<?php

use App\Events\MessagePosted;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use App\Models\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/chat', function () {
    return view('chat');
})->middleware('auth');

Route::get('/getUserLogin', function () {
	return Auth::user();
})->middleware('auth');

Route::get('/messages', function () {
    return Message::with('user')->get();
})->middleware('auth');

Route::post('/messages', function () {
   $user = Auth::user();
  $message = new Message();
  $message->message = request()->get('message', '');
  $message->user_id = $user->id;
  $message->save();

  broadcast(new MessagePosted($message, $user))->toOthers();
  return ['message' => $message->load('user')];
})->middleware('auth');
